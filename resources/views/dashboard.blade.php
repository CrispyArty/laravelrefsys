@extends('layouts.app')

@section('content')
@if ( Auth::user()->isRef() )
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">Реферальная ссылка</div>
                <div class="box-body">
                    <input type="text" class="form-control" readonly value="{{Auth::user()->refUrl()}}">
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Кошелёк</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    @foreach( Auth::user()->balances as $balance)
                        <tr>
                            <th>{{$balance->currency->title}}</th>
                            <td>{{$balance->amount}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@if ( Auth::user()->isRef() )
    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Партнёры</h3>
            </div>
            <div class="box-body">
                @if ( empty(Auth::user()->partners->first()) )
                    У вас пока нет партнёров. Используйте реферальную ссылку чтобы пригласить людей. 
                @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Прибыль - Рубли</th>
                                <th>Прибыль - Доллары</th>
                            </tr>
                        </thead>
                        @foreach( Auth::user()->partners as $partner)
                            <tr>
                                <th>{{$partner->name}}</th>
                                @foreach ( $partner->balances as $balance )
                                    <td>
                                        {{App\Helpers\CurrHelper::refPercent($balance->amount)}}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
@endif
</div>
@endsection
