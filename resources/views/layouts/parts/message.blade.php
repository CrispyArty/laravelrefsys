@if( session('success') )
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{session('success')}}
    </div>
@endif

@if( session('error') )
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif