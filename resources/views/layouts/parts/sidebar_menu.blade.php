<ul class="sidebar-menu tree" data-widget="tree">
    <li class="header">Меню</li>
    <li class="{{ (\Request::route()->named('dashboard')) ? 'active' : '' }}">
        <a href="/dashboard">
            <i class="fa fa-user"></i> <span>Кабинет</span>
        </a>
    </li>
    <li class="{{ (\Request::route()->named('investForm')) ? 'active' : '' }}">
        <a href="/invest">
            <i class="fa fa-money"></i> <span>Пополнение кошелька</span>
        </a>
    </li>
</ul>