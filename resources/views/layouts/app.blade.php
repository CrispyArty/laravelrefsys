<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>{{ config('app.name') }}</title>
        <link rel='stylesheet' href='{{asset('css/app.css')}}'>

        <script src="{{asset('js/app.js')}}"></script>
    </head>
    <body class="hold-transition skin-black sidebar-mini">
        <div class="wrapper">
            <div class="main-header">
                <a href="/" class="logo">
                    <span class="logo-mini">
                        <b>L</b>Ref 
                    </span>
                    <span class="logo-lg">
                        <b>Laravel</b>Ref
                    </span>
                </a>
                <div class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    @auth
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="user-body">
                                            <div class="row">
                                                @foreach( Auth::user()->balances as $balance )
                                                    <div class="col-xs-6">
                                                        {{$balance->amount}} {{$balance->currency->title}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </li>
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="/dashboard" class="btn btn-default btn-flat">Кабинет</a>
                                            </div>
                                            <div class="pull-right">
                                                <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    Выход
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    @endauth
                </div>
            </div>

            <div class="main-sidebar">
                <div class="sidebar">
                    @include('layouts.parts.sidebar_menu')
                </div>
            </div>
            <div class="content-wrapper">
                <div class="content">
                    @include('layouts.parts.message')
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
