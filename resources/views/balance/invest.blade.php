@extends('layouts.app')

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">Пополнение счёта</div>
    {!! Form::open(['action' => 'BalancesController@investSave']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group {{$errors->has('currency_id') ? 'has-error' : ''}}">
                    {!! Form::label('currency_id', 'Валюта', ['class' => 'control-label']) !!}
                    {!! Form::select('currency_id', App\Currency::pluck('title', 'id'), 1, ['class' => 'form-control']) !!}
                    @if ($errors->has('currency_id'))
                        <span class="help-block">{{ $errors->first('currency_id') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group {{$errors->has('amount') ? 'has-error' : ''}}">
                    {!! Form::label('amount', 'Сумма', ['class' => 'control-label']) !!}
                    {!! Form::number('amount', 0.00, ['class' => 'form-control', 'step' => '0.01']) !!}
                    @if ($errors->has('amount'))
                        <span class="help-block">{{ $errors->first('amount') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Пополнить</button>
    </div>
    
    {!! Form::close() !!}
</div>
@endsection