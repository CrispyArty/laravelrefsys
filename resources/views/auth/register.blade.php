@extends('layouts.auth')

@section('content')
<div class="login-box">
    <div class="login-logo">Регистрация</div>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="login-box-body">
            <input type="hidden" name="ref_token" value="{{ $refToken }}">
            <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Имя</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Почта</label>

                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                <label for="password">Пароль</label>
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="password-confirm">Подтверждение пароля</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>

            @if ( isset($refUser) )
                <div class="form-group {{$errors->has('ref_token') ? 'has-error' : ''}}">
                    <div>Вас пригласил: <b class="">{{$refUser->name}}</b></div>
                </div> 
            @endif
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Зарегистрироваться</button>
        </div>
    </form>
</div>
@endsection
