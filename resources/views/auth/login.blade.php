@extends('layouts.auth')

@section('content')
<div class="login-box">
    <div class="login-logo">Вход</div>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="login-box-body">

            <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                <label for="email" class="">Почта</label>

                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                <label for="password" class="">Пароль</label>
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня?
                    </label>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-xs-6">
                    {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a> --}}
                    {{-- <br> --}}
                    <a class="btn btn-link" href="{{ route('register') }}">
                        Регистрация
                    </a>
                </div>
                <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary pull-right">Вход</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
