@extends('layouts.auth')

@section('content')
<div class="login-box">
    <div class="login-logo">Смена пароля</div>
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="login-box-body">

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">Почта</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">{{ __('Send Password Reset Link') }}</button>
        </div>
    </form>
</div>
@endsection
