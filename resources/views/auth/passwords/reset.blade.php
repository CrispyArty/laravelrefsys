@extends('layouts.auth')

@section('content')
<div class="login-box">
    <div class="login-logo">Смена пароля</div>
    <form method="POST" action="{{ route('password.request') }}">
        @csrf
        <div class="login-box-body">
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Почта</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                <label for="password">Пароль</label>
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="password-confirm">Подтверждение пароля</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Поменять пароль</button>
        </div>
    </form>
</div>
@endsection
