<?php

namespace App\Observers;

use App\User;
use App\Currency;
use App\Balance;

class UserObserver
{

    public function creating(User $user)
    {
        if ( empty($user->ref_id) ) {
            $user->ref_token = $this->generateToken($user);
        }
    }
    
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user) {

        $currencies = Currency::all();
        foreach ($currencies as $curr) {
            Balance::create([
                'user_id' => $user->id,
                'currency_id' => $curr->id,
            ]);
        }
    }

    protected function generateToken($user) {
        $i = 0;
        do {
            $token = md5(sha1(microtime(1) . $user->email . $user->name . ++$i));
        } while ( User::where('ref_token', $token)->count() );

        return $token;
    }
}