<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = [
        'currency_id', 'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function currency() {
        return $this->belongsTo('App\Currency');
    }

    public function processAmount($amount) {
        $this->amount += $amount;
        
        $ref = $this->user->ref;
        if ( !empty($ref) ) {
            $refBal = $ref->balances($this->currency_id)->first();
            $refBal->amount += $amount * config('settings.refPercent') / 100;
            $refBal->save();
        }

        $this->save();
    }
}
