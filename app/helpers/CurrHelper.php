<?php 

namespace App\Helpers;

class CurrHelper {

    public static function format($sum) {
        return number_format($sum, 2, '.', ' ');
    }

    public static function refPercent($sum, $format = true) {
        $sum = $sum * config('settings.refPercent') / 100;

        return $format ? self::format($sum) : $sum;
    }
}