<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Observers\UserObserver;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ref_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'ref_token'
    ];
    
    public function balances($currId = null) {
        if ( isset($currId) ) {
            return $this->hasMany('App\Balance')->where('currency_id', $currId);
        } else {
            return $this->hasMany('App\Balance');
        }
    }

    public function partners() {
        return $this->hasMany('App\User', 'ref_id');
    }

    public function ref() {
        return $this->belongsTo('App\User', 'ref_id');
    }

    public function isRef() {
        return $this->ref_id == 0 && !empty($this->ref_token);
    }

    public function refUrl() {
        return route('refRegister', ['refToken' => $this->ref_token]);
    }
}
