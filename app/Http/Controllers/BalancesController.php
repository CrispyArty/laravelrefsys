<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balance;
use App\User;
use App\Currency;

class BalancesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function invest() {
        return view('balance.invest');
    }

    public function investSave(Request $request) {
        $validatedData = $request->validate([
            'currency_id' => 'required|integer',
            'amount' => 'required|numeric',
        ]);

        $balance = Balance::where([
            ['currency_id', $validatedData['currency_id']],
            ['user_id', auth()->user()->id],
        ])->first();

        if ( empty($balance) ) {
            abort(404, 'Что-то пошло не так');
        }

        $balance->processAmount($validatedData['amount']);

        return redirect()->route('dashboard')->with('success', 'Вы пополнили счёт');
    }
}
