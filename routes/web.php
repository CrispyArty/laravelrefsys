<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/invest', 'BalancesController@invest')->name('investForm');
Route::post('/investSave', 'BalancesController@investSave')->name('investSave');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('register/{refToken}', 'Auth\RegisterController@showRegistrationForm')->name('refRegister');
// Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
