<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddRefFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('ref_id')->default(0);
            $table->index('ref_id');
            $table->string('ref_token')->nullable(true);
            $table->index('ref_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('ref_id');
            $table->dropColumn('ref_id');
            $table->dropIndex('ref_token');
            $table->dropColumn('ref_token');
        });
    }
}
